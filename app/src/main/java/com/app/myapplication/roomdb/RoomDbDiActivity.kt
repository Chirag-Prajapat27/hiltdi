package com.app.myapplication.roomdb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityRoomDbDiBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RoomDbDiActivity : AppCompatActivity() {

    lateinit var db : DataBaseStudents

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityRoomDbDiBinding = DataBindingUtil.setContentView(this,R.layout.activity_room_db_di)

        db = DataBaseStudents.getDataBase(this)
        GlobalScope.launch {
            db.daoStudent().insertStudentData(StudentModel("Chirag",22))
        }

        binding.btnReadData.setOnClickListener {
            readData()
        }
    }

    private fun readData() {
        db.daoStudent().getAllStudents().observe(this) {
            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
        }
    }

}