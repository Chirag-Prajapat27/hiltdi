package com.app.myapplication.roomdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [StudentModel::class], version = 2, exportSchema = false)
abstract class DataBaseStudents : RoomDatabase() {
    abstract fun daoStudent(): RoomDao

    companion object {
        @Volatile
        var INSTANCES: DataBaseStudents? = null

        fun getDataBase(context: Context): DataBaseStudents {
            if (INSTANCES != null) {
                synchronized(this) {
                    INSTANCES = Room.databaseBuilder(context.applicationContext,
                        DataBaseStudents::class.java, "student_database"
                    ).build()
                }
            }
            return INSTANCES!!
        }
    }
}