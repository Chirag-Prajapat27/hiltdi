package com.app.myapplication.roomdb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "student_table")
data class StudentModel(
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name= "age") val age: Int,
    @PrimaryKey(autoGenerate = true) val id: Int=0
    )
