package com.app.myapplication.roomdb

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface RoomDao {

    @Query("Select * from student_table")
    fun getAllStudents() : LiveData<List<StudentModel>>

    @Insert
    fun insertStudentData(students: StudentModel)

    @Query("Select * from student_table")
    fun getAllData() : Flow<List<StudentModel>>

}