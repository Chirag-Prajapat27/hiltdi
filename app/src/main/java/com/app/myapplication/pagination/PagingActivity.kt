package com.app.myapplication.pagination

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityPagingBinding
import com.app.myapplication.retrofit.QuoteViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PagingActivity : AppCompatActivity() {

    private val quoteViewModel: QuoteViewModel by viewModels()
    @Inject
    lateinit var adapter: QuotesAdapterPaging

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityPagingBinding = DataBindingUtil.setContentView(this@PagingActivity,R.layout.activity_paging)
        binding.rvQuotesResult.adapter = adapter

        lifecycleScope.launchWhenStarted {
            quoteViewModel.listData.collect {
                Log.d("ApiData",it.toString())
                adapter.submitData(it)
            }
        }


    }
}