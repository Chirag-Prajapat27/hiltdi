package com.app.myapplication.pagination

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.app.myapplication.retrofit.Result

class PagingDataSource(private val retroInterface: RetroInterface) : PagingSource<Int,Result>() {

    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        val currentKey = params.key ?: 1
        val response = retroInterface.getQutesPaging(currentKey)
        val responseData = mutableListOf<Result>()
        val data = response.results?: emptyList()
        responseData.addAll(data)

        val prevKey = if (currentKey==1) null else currentKey-1

        return LoadResult.Page(data,prevKey,currentKey.plus(1))
    }


}