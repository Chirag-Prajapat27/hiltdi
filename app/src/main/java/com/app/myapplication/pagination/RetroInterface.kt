package com.app.myapplication.pagination

import com.app.myapplication.retrofit.QuoteModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RetroInterface {

    @GET("/quotes")
    suspend fun getQutesPaging(@Query("page")page:Int): QuoteModel


    companion object{
        fun getApiService() = Retrofit.Builder().baseUrl("https://api.quotable.io")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RetroInterface::class.java)

    }

}