package com.app.myapplication.constructorinjection

import android.util.Log
import javax.inject.Inject

class SendNotification @Inject constructor() {

    fun showNotification() {
        Log.d("mylog","Show Notifiction")
    }
}