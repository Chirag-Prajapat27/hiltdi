package com.app.myapplication.constructorinjection

import android.util.Log


interface NotificationService {

    fun sendNotification(mag:String)
}

class EmailService : NotificationService {

    override fun sendNotification(mag: String) {
            Log.d("mylog","Email Send successfully")
    }
}

class SmsService : NotificationService {

    override fun sendNotification(mag: String) {
            Log.d("mylog","Notification Send successfully")
    }

}