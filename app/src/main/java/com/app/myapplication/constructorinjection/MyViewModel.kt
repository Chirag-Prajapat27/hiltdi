package com.app.myapplication.constructorinjection

import android.util.Log
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MyViewModel @Inject constructor(val user: User) : ViewModel() {

  fun getViewModel() {
      Log.d("mylog","From ViewModel")
      user.getUser()
  }

}