package com.app.myapplication.constructorinjection

import android.util.Log
import javax.inject.Inject

class User @Inject constructor(private val notiService: NotificationService,
                               private val notificationFunction: SendNotification) {

    fun getUser() {
        Log.d("mylog","It is call")

        //constructor Injection
        notificationFunction.showNotification()

        //constructor Injection using with interface
        notiService.sendNotification("Hello guys, this chirag")

    }

}