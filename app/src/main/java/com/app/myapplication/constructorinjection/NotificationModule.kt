package com.app.myapplication.constructorinjection

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class NotificationModule {

    @Provides
    fun sendMessage() : NotificationService {
        return EmailService()
    }

}