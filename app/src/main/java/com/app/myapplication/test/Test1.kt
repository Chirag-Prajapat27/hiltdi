package com.app.myapplication.test

import kotlinx.coroutines.*

fun main() = runBlocking {

    CoroutineScope(Dispatchers.IO).launch {
        printData()
    }
    Thread.sleep(3000)
}

suspend fun printData() {
    println("MYDATA :: ${stu()}")
}

suspend fun stu() : String {
    delay(2000)
    return "Hello"
}