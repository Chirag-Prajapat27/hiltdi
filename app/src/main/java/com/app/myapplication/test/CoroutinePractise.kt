package com.app.myapplication.test

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun main()  {

    println("Main Thread is working "+Thread.currentThread().name)

    GlobalScope.launch {
        println("Fake work is start "+Thread.currentThread().name)
        delay(1000)
        println("Fake work is end")
    }
    Thread.sleep(2000)
    println("Main work is start "+Thread.currentThread().name)

}