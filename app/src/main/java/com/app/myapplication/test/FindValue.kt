package com.app.myapplication.test

import kotlin.collections.*
import kotlin.io.*
import kotlin.ranges.*
import kotlin.text.*

fun matchingStrings(strings: Array<String>, queries: Array<String>): Array<Int> {
    // Write your code here

    val indexCount  = ArrayList<Int>()
    var count = 0

    for (i in 0..queries.size) {
        for (j in 0..strings.size) {
            if(queries[i].equals( strings[j])){
                count++
            }
            indexCount.add(count)
        }
    }
    return indexCount.toTypedArray()

}

fun main() {
    val stringsCount = readLine()!!.trim().toInt()

    val strings = Array<String>(stringsCount, { "" })
    for (i in 0 until stringsCount) {
        val stringsItem = readLine()!!
        strings[i] = stringsItem
    }

    val queriesCount = readLine()!!.trim().toInt()

    val queries = Array<String>(queriesCount, { "" })
    for (i in 0 until queriesCount) {
        val queriesItem = readLine()!!
        queries[i] = queriesItem
    }

    val res = matchingStrings(strings, queries)

    println(res.joinToString("\n"))
}
