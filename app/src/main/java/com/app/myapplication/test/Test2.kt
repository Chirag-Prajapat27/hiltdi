package com.app.myapplication.test

import kotlinx.coroutines.*

fun main() {

    CoroutineScope(Dispatchers.IO).launch {
//        executeTask()
        executeBlock()
//        asyncTask()
    }
    runBlocking {
        delay(2000)
    }

}

suspend fun asyncTask() {
    CoroutineScope(Dispatchers.IO).async {
        delay(2000)
        println("This is Async")
    }
}

suspend fun executeTask() {
    println("Before")

    GlobalScope.launch {
        delay(1000)
        println("Inside")
    }
    println("After")
}


suspend fun executeBlock() {
    println("Before")

    withContext(Dispatchers.IO) {
        delay(1000)
        println("Inside")
    }
    println("After")
}
