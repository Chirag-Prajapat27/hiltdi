package com.app.myapplication.paginthilt

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PagingHiltViewModel @Inject constructor(val pagingSource: PagingDataSourceHilt) :ViewModel() {

    val listData = Pager(PagingConfig(6)) {
        pagingSource
    }.flow

}