package com.app.myapplication.paginthilt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityPagingHiltBinding
import com.app.myapplication.pagination.QuotesAdapterPaging
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PagingHiltActivity : AppCompatActivity() {

    private val pagingHiltViewModel : PagingHiltViewModel by viewModels()
    @Inject
    lateinit var adapter : QuotesAdapterPaging

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_paging_hilt)

        val binding : ActivityPagingHiltBinding = DataBindingUtil.setContentView(this@PagingHiltActivity,R.layout.activity_paging_hilt )
        binding.rvQuotesResult.adapter = adapter

        lifecycleScope.launchWhenStarted {
            pagingHiltViewModel.listData.collect {
                Log.d("Data","Hilt Data paging $it")
                adapter.submitData(it)
            }
        }

    }
}