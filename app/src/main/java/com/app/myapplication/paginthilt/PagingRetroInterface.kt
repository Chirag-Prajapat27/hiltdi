package com.app.myapplication.paginthilt

import com.app.myapplication.retrofit.QuoteModel
import retrofit2.http.GET
import retrofit2.http.Query

interface PagingRetroInterface {

    @GET("quotes")
    suspend fun getAllQuotes(@Query("page")page:Int) : QuoteModel
}