package com.app.myapplication.notificationlearn.model

data class FCMResponse(
    private val multicast_id: Long = 0,
    private val success : Int = 0,
    private val failure : Int = 0,
    private val cononical_ids : Int = 0,
    private var results: List<Result>? = null
)
data class Result (private var message_id: String? = "")