package com.app.myapplication.notificationlearn.model

data class FCMSendModel(
    val to: String,
    var priority: String,
    val data: Data? = null,
    val notification: Notification,
)

data class Data(
    val dl: String? = null,
    val url: String
)

data class Notification(
    val body: String,
//    val mutable_content: Boolean = true,
//    val sound: String,
    val title: String
)