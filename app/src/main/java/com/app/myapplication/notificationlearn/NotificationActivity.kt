package com.app.myapplication.notificationlearn

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityNotificationBinding
import com.app.myapplication.notificationlearn.model.Data
import com.app.myapplication.notificationlearn.model.FCMSendModel
import com.app.myapplication.notificationlearn.model.Notification
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

//redmi y2 FCM token :: dHoNNY6wRFCq_Bw5qMKXO7:APA91bG02eaS8CEvm935blWSnAvoX4Q3gzVEUgQcmLQQvHMtncFYWIVReKKdasgIG7qaLBfQeMhLz4kFkxiUjl_X5Sq6jbBViUoWdqgjTpFQVgKGjMjcdKfEsyeF8FgWyxDLT-n_6zB2
const val ANDROID = "android"
const val FLUTTER = "flutter"
const val IOS = "ios"

class NotificationActivity : AppCompatActivity() {

    lateinit var binding : ActivityNotificationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_notification)

        getFcmToken()
        allClick(binding)

//        val fm = Firebase.messaging
//        fm.send(remoteMessage("$SENDER_ID@fcm.googleapis.com") {
//            setMessageId(messageId.toString())
//            addData("my_message", "Hello World")
//            addData("my_action", "SAY_HELLO")
//        })
    }

    private fun allClick(binding: ActivityNotificationBinding?) {
        binding!!.btnAndroid.setOnCheckedChangeListener { _, isChecked  ->
            if(isChecked) topicSubscribe(ANDROID) else topicUnsubscribe(ANDROID)
        }

        binding.btnFlutter.setOnCheckedChangeListener {  _, isChecked  ->
            if(isChecked) topicSubscribe(FLUTTER) else topicUnsubscribe(FLUTTER)
        }

        binding.btnIOS.setOnCheckedChangeListener {  _, isChecked  ->
            if(isChecked) topicSubscribe(IOS) else topicUnsubscribe(IOS)
        }

        //for Send Notification
        binding.btnAndroidSend.setOnClickListener {
//            sendNotification("Hello From App", "Hii guys i'm chirag prajapat from Rajasthan")
            sendTopicMessage(ANDROID,"This is Android topic from app", "Hii guys i'm chirag prajapat from Rajasthan")
        }

        binding.btnFlutterSend.setOnClickListener {
            sendTopicMessage(FLUTTER,"This is Flutter topic from app", "Hii guys i'm chirag prajapat from Rajasthan")
        }

        binding.btnIOSSend.setOnClickListener {
            sendTopicMessage(IOS,"This is IOS topic from app", "Hii guys i'm chirag prajapat from Rajasthan")
        }
    }

    private fun sendTopicMessage(topic: String, title: String, body: String) =
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val responseBody = FCMSendModel("/topics/$topic", "high", data= Data(url = "https://miro.medium.com/max/700/0*rZecOAy_WVr16810"),  notification = Notification(body, title))

                val response = NotificationRetrofit.apiService.sendNotification(responseBody)
                if (response.isSuccessful) {
                    Toast.makeText(this@NotificationActivity, "send notification Successful", Toast.LENGTH_SHORT).show()
                    Log.d("FCMmsg", "Successful : ${response.message()}")
                } else {
                    //here I get 404
                    Log.d("FCMmsg", "Error ${response.code()}")
                }

            } catch (e: Exception) {
                Log.d("FCMLOg", "Exception $e")
            }

        }

    private fun topicSubscribe(topicName: String) {
        Firebase.messaging.subscribeToTopic(topicName)
            .addOnCompleteListener { task ->
                var msg = "Subscribed $topicName"
                if (!task.isSuccessful) {
                    msg = "Subscribe failed"
                }
                Log.d("FCMMsg", msg)
                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            }
    }

    private fun topicUnsubscribe(topicName: String) {
        Firebase.messaging.unsubscribeFromTopic(topicName)
            .addOnCompleteListener { task ->
                var msg = "Unsubscribed $topicName"
                if (!task.isSuccessful) {
                    msg = "Unsubscribe failed"
                }
                Log.d("FCMMsg", msg)
                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            }
    }

    private fun getFcmToken() {

        FirebaseMessaging.getInstance().token.addOnSuccessListener { token ->
            if (!TextUtils.isEmpty(token)) {
                Log.d("FCMToken", "retrieve token successful : $token")
            } else {
                Log.d("FCM", "token should not be null...")
            }
        }.addOnFailureListener {
            Log.d("FCM", "FailureListener")
        }.addOnCompleteListener { task ->
            Log.d("FCM", "CompleteListener")
            //get new FCM registration token
            Log.d("FCMToken", "FCM Token :: ${task.result}")
        }
    }

/*    private fun sendNotification(body:String,title:String)= CoroutineScope(Dispatchers.IO).launch {
        try {
            //orders is the topic name
            val message = Message("/topics/"+ANDROID,Notification(body,title))
            val response = NotificationRetrofit.apiService.sendNotification(message)

            if (response.isSuccessful) {
                Log.e("msg1", response.message())
            } else {
                //here i get 404
                Log.e("msg", response.code().toString())
            }
        } catch (e: Exception) {
            Log.e("Error", e.message.toString())
        }

    }*/

}