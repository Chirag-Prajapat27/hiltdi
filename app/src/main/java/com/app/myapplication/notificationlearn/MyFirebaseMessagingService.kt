package com.app.myapplication.notificationlearn

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.app.myapplication.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService :  FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("fcmToken", token)

//        SharedPreference.setValue(SharedPreference.FCM_TOKEN, token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Log.d("firebase", message.data.toString())
        if(applicationInForeground()) {
            sendNotification(message)
        }
    }

    //check app in foreground
    private fun applicationInForeground(): Boolean {
        var isActivityFound = false
        if ((getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                .runningAppProcesses[0].processName
                .equals(packageName, ignoreCase = true)
        ) {
            isActivityFound = true
        }
            return isActivityFound
    }

    //Send notification
    private fun sendNotification(message: RemoteMessage) {
        val intent = Intent().apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getActivity(  this,
                (System.currentTimeMillis() and 0xffffffff).toInt(),
                intent, PendingIntent.FLAG_MUTABLE )
        } else {
            PendingIntent.getActivity(  this,
                (System.currentTimeMillis() and 0xffffffff).toInt(),intent,PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val notificationChannelID = resources.getString(R.string.app_name)
        val notificationBuilder = NotificationCompat.Builder(this,notificationChannelID)
            .setWhen(System.currentTimeMillis())
            .setShowWhen(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setContentTitle(message.notification?.title)
            .setContentText(message.notification?.body)
            .setLights(Color.BLUE,1,0 )
            .setVibrate(longArrayOf(0, 1000, 500, 1000))
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE or Notification.DEFAULT_LIGHTS)
            .setColor(ContextCompat.getColor(applicationContext,R.color.white))
            .setAutoCancel(true)
            .setGroupSummary(true)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH

            val notificationChannel =
                NotificationChannel(notificationChannelID, notificationChannelID, importance)
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC

            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.enableVibration(true)
            notificationBuilder.setChannelId(notificationChannelID)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        notificationManager.notify((System.currentTimeMillis() and 0xfffffff).toInt(), notificationBuilder.build())
    }

}
