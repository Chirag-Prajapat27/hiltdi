package com.app.myapplication.notificationlearn

import com.app.myapplication.notificationlearn.model.FCMResponse
import com.app.myapplication.notificationlearn.model.FCMSendModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface FCMApiInterface {

    @POST("fcm/send")
    suspend fun sendNotification(@Body body: FCMSendModel?): Response<FCMResponse>

}