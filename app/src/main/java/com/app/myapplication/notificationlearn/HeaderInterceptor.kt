package com.app.myapplication.notificationlearn

import okhttp3.Interceptor
import okhttp3.Response

const val FCM_AUTO_KEY = "AAAAc-TyJOk:APA91bEgUOxyAKQXcRo39I0fW3e1EJlmICt0-HkvolzqTARtV50e6JfocmarhjQYmy4sJEmD1B6yeQPNeKj_M55WqZthrp-h9iGnft-few93vFVn1DHXVzOkBTLsQUik6BV-xg_TvYNh"

internal class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
            .addHeader("Content-Type", "application/json")
        if (FCM_AUTO_KEY.isNotEmpty()) {
            builder.addHeader("Authorization", "key= $FCM_AUTO_KEY")
        }
        return chain.proceed(builder.build())
    }
}
