package com.app.myapplication.retrofit

import com.app.myapplication.paginthilt.PagingRetroInterface
import com.app.myapplication.practice.retrofit.PracticeRetrofitService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class RetrofitServiceBuilder {  

    @Provides
    fun providesBaseUrl() : String = "https://api.quotable.io/"

    private val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    fun okHttpClientBuilder() : OkHttpClient.Builder =OkHttpClient.Builder().addInterceptor(logger)

    @Provides
    fun retrofitBuilder(baseUrl : String, okHttpClient: OkHttpClient.Builder) : Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient.build())
        .build()

//    @Provides
//    fun <T> quotesApis(retrofit: Retrofit) : T = retrofit.create(T::class.java)

    @Provides
    fun providesQuotesApis(retrofit: Retrofit) :RetrofitService = retrofit.create(RetrofitService::class.java)


    @Provides
    fun myProvidesQuotesApis(myRetrofit: Retrofit) : PracticeRetrofitService = myRetrofit.create(PracticeRetrofitService::class.java)

    @Provides
    fun providersQuotesApi(retrofit: Retrofit) : PagingRetroInterface = retrofit.create(PagingRetroInterface::class.java)

//    @Provides
//    fun <T> buildService(serviceType: Class<T>,retrofit: Retrofit): T {
//        return Retrofit.create(serviceType)
//    }

//    @Provides
//    fun <T> providersTest(serviceType: Class<T>, retrofit: Retrofit) : Class<T> = retrofit.create(serviceType::class.java)

}
