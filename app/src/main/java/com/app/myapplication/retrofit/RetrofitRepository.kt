package com.app.myapplication.retrofit

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RetrofitRepository @Inject constructor(private val service: RetrofitService) {

     suspend fun getQuoteList() : Flow<QuoteModel> = flow {
        val response = service.getQuotesList()
        emit(response)
    }
}
