package com.app.myapplication.retrofit

import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    @GET("quotes")
    suspend fun getQuotesList() : QuoteModel

    @GET("quotes")
    suspend fun getQutesPaging(@Query("page")page:Int): QuoteModel

}