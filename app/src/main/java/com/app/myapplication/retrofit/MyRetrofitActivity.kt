package com.app.myapplication.retrofit

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityMyretrofitBinding
import com.app.myapplication.retrofit.loadingmanage.NetworkResult
import com.app.myapplication.utility.CustomDialog
import com.app.myapplication.utility.ToastHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@AndroidEntryPoint
class MyRetrofitActivity : AppCompatActivity() {

    @Inject
    lateinit var repository: RetrofitRepository

    private lateinit var binding: ActivityMyretrofitBinding

    private val quoteViewModel: QuoteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_myretrofit)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_myretrofit)
        binding.vm = quoteViewModel

        quoteViewModel.userLiveData.observe(this) { response ->
            when(response) {
                is NetworkResult.Success -> {
                    response.data?.let {
                        ToastHelper.showMessage(this,it.toString())
                    }
                }
                is NetworkResult.Error -> {
                    CustomDialog.getInstance().hide()
                }
                is NetworkResult.Loading -> {
                    CustomDialog.getInstance().showDialog(this)
                }
            }

        }

//        quoteViewModel.getData().observe(this) { loadState->
//            quoteViewModel.loadingState.postValue(LoadingSatate.Loading)
//            loadState.getValueOrNull()?.let {
//                quoteViewModel.loadingState.postValue(LoadingSatate.Loaded)
//                Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
//
//            }
//        }


//        quoteViewModel.getData()
//        quoteViewModel.getVmQuotes.observe(this) {
//            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
//            Log.d("mylogRtro", "$it")
//        }

//        myFlow()
    }

    // Extra Practice
    private fun myFlow() {

        val myFlowVar: Flow<Int> = flow {
            for (i in 0..4)
                emit(i)

        }.flowOn(Dispatchers.IO)

        runBlocking {
            myFlowVar.collect {
                Toast.makeText(this@MyRetrofitActivity, it.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

}