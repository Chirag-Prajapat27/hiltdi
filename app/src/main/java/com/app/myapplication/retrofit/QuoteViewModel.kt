package com.app.myapplication.retrofit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.app.myapplication.pagination.PagingDataSource
import com.app.myapplication.pagination.RetroInterface
import com.app.myapplication.retrofit.loadingmanage.LoadingSatate
import com.app.myapplication.retrofit.loadingmanage.NetworkResult
import com.app.myapplication.retrofit.loadingmanage.toLoadingState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import javax.inject.Inject

@HiltViewModel
class QuoteViewModel @Inject constructor(private val repository: RetrofitRepository): ViewModel() {

    var loadingState = MutableLiveData<LoadingSatate>(LoadingSatate.Loading)

//    private val _userResponseLiveData = MutableLiveData<NetworkResult<QuoteModel>>()
    private val _userResponseLiveData : MutableLiveData<NetworkResult<QuoteModel>> = MutableLiveData()


    fun getData() = liveData(Dispatchers.IO) {
        repository.getQuoteList().toLoadingState()
            .catch {
                _userResponseLiveData.postValue(NetworkResult.Error(it.message))
            }.collect {
                _userResponseLiveData.postValue(NetworkResult.Loading())
                _userResponseLiveData.postValue(NetworkResult.Success(it.getValueOrNull()))
            emit(it)
        }
    }

    val userLiveData : LiveData<NetworkResult<QuoteModel>>
        get() = _userResponseLiveData


    val listData = Pager(PagingConfig(6)) {

        PagingDataSource(RetroInterface.getApiService())
    }.flow


//    fun getData() {
//        viewModelScope.launch {
//            repository.getQuoteList().collect {
//                getVmQuotes.value = it
//            }
//        }
//    }

//    fun getVMQuotes() = liveData {
//        repository.getQuoteList().catch { e ->
//        }.collect {
//            emit(it)
//        }
//    }

}