package com.app.myapplication.roommvvm

import androidx.lifecycle.ViewModel
import com.app.myapplication.roomdb.StudentModel
import kotlinx.coroutines.flow.Flow

class RoomViewModel(val repository: RoomRepository) :ViewModel() {

    suspend fun getData() : Flow<List<StudentModel>> = repository.getAllStudent()

}