package com.app.myapplication.roommvvm

import com.app.myapplication.roomdb.RoomDao
import com.app.myapplication.roomdb.StudentModel
import kotlinx.coroutines.flow.Flow

class RoomRepository(private val dao: RoomDao) {

    fun getAllStudent() : Flow<List<StudentModel>> = dao.getAllData()


}