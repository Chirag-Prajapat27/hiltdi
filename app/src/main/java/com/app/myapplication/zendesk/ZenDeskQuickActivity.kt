package com.app.myapplication.zendesk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.app.myapplication.R
import zendesk.core.AnonymousIdentity
import zendesk.core.Identity
import zendesk.core.Zendesk
import zendesk.support.Support
import zendesk.support.guide.HelpCenterActivity

class ZenDeskQuickActivity : AppCompatActivity() {

    private lateinit var btnZendesk : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zen_desk_quick)

        Zendesk.INSTANCE.init(
            this, "https://getplus.zendesk.com",
            "dd53fe66cde7b9d94d4c44cf385e5a1f02010fd66de24772",
            "mobile_sdk_client_a511d750d7ed3338adbd"
        )

        val identity: Identity = AnonymousIdentity()
        Zendesk.INSTANCE.setIdentity(identity)

        Support.INSTANCE.init(Zendesk.INSTANCE)

        btnZendesk = findViewById(R.id.btnZendesk)

        btnZendesk.setOnClickListener {
            HelpCenterActivity.builder()
                .show(this)
        }
    }
}