package com.app.myapplication.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.app.myapplication.R
import kotlin.concurrent.thread

class ForGroundService : Service()  {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("MyCounter", "Start Service")
        thread {
            var counter = 0
            while (true) {
                Thread.sleep(1000)
                counter++
                Log.d("MyCounter", "Counter = $counter")
            }
        }

        val channelId = "HiltDI"
        getSystemService(NotificationManager::class.java).createNotificationChannel(
            NotificationChannel(channelId,channelId,NotificationManager.IMPORTANCE_LOW)
        )

        val notification = Notification.Builder(this,channelId).setContentText("Service")
            .setContentTitle("Service is running...")
            .setSmallIcon(R.mipmap.ic_launcher).build()

        //Background to foreground service
        startForeground(101,notification)

        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}