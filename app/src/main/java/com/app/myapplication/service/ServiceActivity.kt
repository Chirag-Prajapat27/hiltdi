package com.app.myapplication.service

import android.app.ActivityManager
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.myapplication.R
import okhttp3.internal.isSensitiveHeader

class ServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)
    }

    fun runService(view: View) {
        val intent = Intent(this, ForGroundService::class.java)
//        if (!isServiceRunning())   // is Already start or not
        startService(intent)

    }

    fun stopService(view: View) {
        val intent = Intent(this, ForGroundService::class.java)
        stopService(intent)
    }

    private fun isServiceRunning(): Boolean {
        val activityManager = getSystemService(ACTIVITY_SERVICE) as ActivityManager

        for (service: ActivityManager.RunningServiceInfo in activityManager.getRunningServices(Int.MAX_VALUE)) {

            if (MyBackgroundService::class.java.name.equals(service.service.className))
                return true
        }
        return false
    }

}