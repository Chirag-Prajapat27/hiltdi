package com.app.myapplication.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.provider.Settings
import com.app.myapplication.R

class MusicService : Service() {

    private lateinit var mediaPlayer : MediaPlayer
    private val binder = MyIBinder()

    override fun onBind(p0: Intent?): IBinder {
        mediaPlayer = MediaPlayer.create(this,Settings.System.DEFAULT_RINGTONE_URI)
        mediaPlayer.isLooping = true
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

//        mediaPlayer = MediaPlayer.create(this,Settings.System.DEFAULT_RINGTONE_URI)
//        mediaPlayer.isLooping = true
//        mediaPlayer.start()

        val channelId = this.resources.getString(R.string.app_name)
        getSystemService(NotificationManager::class.java).createNotificationChannel(
            NotificationChannel(channelId,channelId,NotificationManager.IMPORTANCE_LOW)
        )

        val notification = Notification.Builder(this,channelId).setContentText("Music Player")
            .setContentTitle(if(this.isPlay())"Play" else "Pause")
            .setSmallIcon(if (this.isPlay())R.drawable.ic_pause else R.drawable.ic_play_arrow)
            .build()

        //BackGround to Foreground Service
        startForeground(1002,notification)

        return START_STICKY
    }

    inner class MyIBinder : Binder() {

        fun getService() : MusicService {

            return this@MusicService
        }
    }

    fun musicPlay() {
        mediaPlayer.start()
    }

    fun musicPause() {
        mediaPlayer.pause()
    }

    fun isPlay() : Boolean {
        return mediaPlayer.isPlaying
    }

    fun musicStop() {
        mediaPlayer.stop()
    }



}