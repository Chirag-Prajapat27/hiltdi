package com.app.myapplication.service

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import kotlin.concurrent.thread

class MyBackgroundService : Service() {

    lateinit var musicPlayer: MediaPlayer

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

//        thread {
//            Thread.sleep(1000)
//            var counter = 0
//            while (true) {
//                counter++
//                Log.d("Counter", "Counter = $counter")
//            }
//        }

        musicPlayer = MediaPlayer.create(this,Settings.System.DEFAULT_RINGTONE_URI)
        musicPlayer.isLooping = true
        musicPlayer.start()
        return START_STICKY
    }

    override fun onDestroy() {
        musicPlayer.stop()
        super.onDestroy()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}