package com.app.myapplication.service

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import androidx.databinding.DataBindingUtil
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityMyBindServiceBinding

class MyBindServiceActivity : AppCompatActivity() {

    var isBound:Boolean = false
    lateinit var myBindService : MyBindService

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(p0: ComponentName?, service: IBinder?) {
            val binder = service as MyBindService.MyIBinder

            myBindService = binder.getService()
            isBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityMyBindServiceBinding = DataBindingUtil.setContentView(this,R.layout.activity_my_bind_service)

        binding.btnStart.setOnClickListener {

            val intent = Intent(this,MyBindService::class.java)
            bindService(intent,connection, BIND_AUTO_CREATE)
        }

        binding.btnStope.setOnClickListener {
            if (isBound) {
                binding.tv.text = myBindService.counter.toString()
            }
        }

    }
}