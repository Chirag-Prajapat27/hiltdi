package com.app.myapplication.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import kotlin.concurrent.thread

class MyBindService : Service() {

    private val binder = MyIBinder()
    var counter = 0

    override fun onBind(p0: Intent?): IBinder {
        Log.d("ServiceLog","Service is Start")

        thread {

            while (true) {

                Thread.sleep(1000)
                counter++
                Log.d("ServiceLog","$counter")
            }
        }
        return binder
    }

   inner class MyIBinder : Binder() {

       fun getService() : MyBindService {

           return this@MyBindService
       }
   }

}