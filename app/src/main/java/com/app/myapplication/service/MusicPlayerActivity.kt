package com.app.myapplication.service

import android.app.job.JobService
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import androidx.databinding.DataBindingUtil
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityMusicPlayerBinding

class MusicPlayerActivity : AppCompatActivity() {

    lateinit var service : MusicService
    var isBound:Boolean = false

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, iBinder: IBinder?) {
            val binding = iBinder as MusicService.MyIBinder

            service = binding.getService()
            isBound = true
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityMusicPlayerBinding = DataBindingUtil.setContentView(this,R.layout.activity_music_player)
        runService()
//        service = MusicService()


        binding.btnPlayPause.setOnClickListener {
//            runService()
//            service = MusicService()
            if (service.isPlay()) {
                service.musicPause()
                binding.btnPlayPause.text = "Play"
                binding.btnPlayPause.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_play_arrow,0)
            } else {
                service.musicPlay()
                binding.btnPlayPause.text = "Pause"
                binding.btnPlayPause.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_pause,0)
            }

        }

        binding.btnStop.setOnClickListener {
//            val service = MusicService()
            service.musicStop()
//            stopService()
            binding.btnPlayPause.text = "Play"
            binding.btnPlayPause.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_play_arrow,0)
        }

    }

    private fun runService() {
        val intent = Intent(this, MusicService::class.java)
        bindService(intent,connection, BIND_AUTO_CREATE)
//        startService(intent)

    }

    private fun stopService() {
        val intent = Intent(this, MusicService::class.java)
        stopService(intent)
    }

}