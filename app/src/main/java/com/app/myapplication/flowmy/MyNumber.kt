package com.app.myapplication.flowmy

data class MyNumber(val number: Int)

data class Product(val id : Int, val name : String, val price: Int, val qty: Int) {

    fun getTotal(): Int {
        return price * qty
    }
}

data class Category(var name: String, val productList: List<Product>) {
    fun getSortBy() : List<Product> {
        return productList.sortedBy { it.price }
    }

    fun getSortByDescending() : List<Product> {
        return productList.sortedByDescending { it.price }
    }
}

