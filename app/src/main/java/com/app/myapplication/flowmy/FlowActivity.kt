package com.app.myapplication.flowmy

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.app.myapplication.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.reduce
import kotlinx.coroutines.launch

class FlowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flow)
        val tv = findViewById<TextView>(R.id.tv)

        val counts = arrayListOf(1,2,3,4,5,6,7,8,9,10).asFlow()

        val apple = Product(1, "Apple", 100,10)
        val banana = Product(2, "Banana", 200,5)
        val berry = Product(3, "berry", 20,20)
        val mango = Product(4, "mango", 50,5)
        val strawBerry = Product(5, "StrawBerry", 150,30)
        val doubleApple = Product(6, "Double Apple", 500,10)
        val product = listOf(apple,banana,berry,mango,strawBerry,doubleApple).asFlow()

        val product1 = listOf(apple,banana,berry,mango,strawBerry,doubleApple)
        val category = listOf(Category("fruit",product1)).asFlow()

    /*    var total = 0
        for (totalCount in 0..counts.size) {
            total += totalCount
            Log.d("MyLogCount", "$total")
        }*/

        //for total price
        CoroutineScope(Dispatchers.IO).launch {
            product.collectLatest {
                Log.d("MyLog","${it.name}: ${it.getTotal()}")
            }

            //for sorting
            CoroutineScope(Dispatchers.IO).launch {
                category.collect {
                        Log.d("MyLogAscending", "${it.name}: ${it.getSortBy()}\n")
                        Log.d("MyLogDescending", "${it.name}: ${it.getSortByDescending()}\n")
                }

            }
            var total = 0
            counts.collect {
                total += it
                Log.d("MyLogCount", "TOTAL COUNT: $total")
            }

            // for Reduce
            val delivery1 = product.reduce { accumulator, value ->
                Product(101,"Package", accumulator.getTotal() + value.getTotal(), accumulator.qty + value.qty )
            }
            Log.d("MyLog","Reduce: $delivery1")

        }



//Test============Test============

//        numbers.asFlow().onEach {
//
//        }.onStart {
//
//        }.flowOn(Dispatchers.IO)

//        CoroutineScope(Dispatchers.IO).launch {
//            var  totalCount = numbers.count {
//                it.number%2==0
//            }
//
//
//        }
//===============================================
    }

}