package com.app.myapplication.practice.retrofit

import retrofit2.http.GET

interface PracticeRetrofitService {

    @GET("quotes")
    suspend fun getQuotes() : TestQuotesModel

}