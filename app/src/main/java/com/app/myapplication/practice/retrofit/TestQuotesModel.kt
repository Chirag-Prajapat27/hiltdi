package com.app.myapplication.practice.retrofit

data class TestQuotesModel(
    val count: Int,
    val lastItemIndex: Int,
    val page: Int,
    val results: List<TestQuotesResult>,
    val totalCount: Int,
    val totalPages: Int
)

data class TestQuotesResult(
    val _id: String,
    val author: String,
    val authorSlug: String,
    val content: String,
    val dateAdded: String,
    val dateModified: String,
    val length: Int,
    val tags: List<String>
)