package com.app.myapplication.practice.retrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import javax.inject.Inject

@HiltViewModel
class PracticeViewModel @Inject constructor(private val repository: PracticeRepository) : ViewModel() {

    fun getQuotes() = liveData(Dispatchers.IO) {
        repository.getQuotes()
            .catch {  }.collect {
                emit(it)
            }
    }

}