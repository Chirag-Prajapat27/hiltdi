package com.app.myapplication.practice.retrofit

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class PracticeRepository @Inject constructor(private val service: PracticeRetrofitService) {

    suspend fun getQuotes() : Flow<TestQuotesModel> = flow {
        val response = service.getQuotes()
        emit(response)
    }
}