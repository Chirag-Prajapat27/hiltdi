package com.app.myapplication.practice.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import com.app.myapplication.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PracticeRetrofitActivity : AppCompatActivity() {

    private val viewModel: PracticeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice_retrofit)

        viewModel.getQuotes().observe(this) { data->
            data!!.let{
                Log.d("DataNew","MyData:: $it")
                Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
            }
        }

    }
}