package com.app.myapplication.remoteconfig

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.app.myapplication.R
import com.app.myapplication.databinding.ActivityRemoteConfigBinding
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings

class RemoteConfigActivity : AppCompatActivity() {

    lateinit var binding : ActivityRemoteConfigBinding
    val remoteConfig : FirebaseRemoteConfig = Firebase.remoteConfig
    val remoteConfigInt  = FirebaseRemoteConfig.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_remote_config)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_remote_config)



        val configSetting = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }

        remoteConfig.setConfigSettingsAsync(configSetting)

        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) {task->
                if (task.isSuccessful) {
                    val updated = task.result
                    val name = remoteConfig.getString("test")
                    Log.d("newValue","Result:: ${name}")
                    Log.d("MyFireBase","Config params updated: $updated")
                    Toast.makeText(this, "Fetch and activate succeeded", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Fetch failed", Toast.LENGTH_SHORT).show()
                }
            }

    }
}